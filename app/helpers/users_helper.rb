module UsersHelper

  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(*parametrs)
    gravatar_id = Digest::MD5::hexdigest(parametrs[0].email.downcase)
    if parametrs[1]
      gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{parametrs[1]}"
    else
      gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=80"
    end
    image_tag(gravatar_url, alt: parametrs[0].name, class: "gravatar")
  end
end
