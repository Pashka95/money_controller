class AccountsController < ApplicationController

  def index
    @accounts = current_user.accounts
  end

  def show
    @account = Account.find(params[:id])
  end

  def new
    @account = Account.new
  end

  def edit
    @account = Account.find(params[:id])
  end

  def create
    @account = Account.new(account_params)
    @account.user_id = current_user.id
    if @account.save
      flash[:success] = "Счет #{@account.name} создан"
      redirect_to user_account_path(current_user, @account)
    else
      render 'new'
    end
  end

  def update
    @account = Account.find(params[:id])

    if @account.update(account_params)
      flash[:success] = "Счет #{@account.name} изменен"
      redirect_to user_account_path
    else
      render 'edit'
    end
  end

  def destroy
    @account = Account.find(params[:id])
    @account.destroy

    flash[:success] = "Счет удален"
    redirect_to user_accounts_path
  end

  def transfer
    @accounts = current_user.accounts
  end

  def create_transfer
    @accounts = current_user.accounts
    if params[:account][:from] == params[:account][:to]
      redirect_to user_accounts_path(current_user)
    else
      @account_from = Account.find(params[:account][:from])
      @account_to = Account.find(params[:account][:to])
      @amount = params[:account][:amount].to_i
      if (@amount) >= 0
        @account_from.amount -= @amount
        @account_to.amount += @amount
        @account_from.save
        @account_to.save
        redirect_to user_accounts_path(current_user)
      else
        render 'transfer'
      end
    end
  end

  private

    def account_params
      params.require(:account).permit(:name, :amount)
    end

end
