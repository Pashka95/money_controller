class PredictedIncomesController < ApplicationController

  def index
    @summa = 0
    @predicted_summa = 0
    @income_categories = IncomeCategory.where(user_id: current_user.id)
    @incomes_array = {}
    @predicted_incomes = {}
    if params[:date]
      @date = Date.new(params[:date][:year].to_i,
                      params[:date][:month].to_i)
    else
      @date = Date.today
    end
    @income_categories.each do |category|
      category.amount = 0
      res = find_by_date(category,@date)
      @incomes_array[category.name], @header, @predicted_incomes[category.name] = res[0], res[1], res[2]
      @incomes_array[category.name].each {|income| category.amount += income.value}
      @summa += category.amount
      @predicted_summa += @predicted_incomes[category.name].amount if @predicted_incomes[category.name]
    end
  end

  def new
    @predicted_income = PredictedIncome.new
    @category = IncomeCategory.find(params[:category])
    date_params = params[:date].split("-")
    @date = Date.new(date_params[0].to_i, date_params[1].to_i, date_params[2].to_i)
  end

  def edit
    @predicted_income = PredictedIncome.find(params[:id])
  end

  def create
    @predicted_income = PredictedIncome.new(predicted_income_params)
    @predicted_income.income_category_id = params[:category]
    date_params = params[:date].split("-")
    @date = Date.new(date_params[0].to_i, date_params[1].to_i, date_params[2].to_i)
    @predicted_income.beginning_of_month = @date.beginning_of_month
    @predicted_income.end_of_month = @date.end_of_month
    if @predicted_income.save
      flash[:success] = "Предсказанный доход создан"
      redirect_to predicted_incomes_path
    else
      render 'new'
    end
  end

  def update
    @predicted_income = PredictedIncome.find(params[:id])
    if @predicted_income.update(predicted_income_params)
      flash[:success] = "Предсказанный доход изменен"
      redirect_to predicted_incomes_path
    else
      render 'edit'
    end
  end

  def destroy
    @predicted_incomes = PredictedIncome.find(params[:id])
    @predicted_incomes.destroy

    flash[:success] = "Предсказаный доход удален"
    redirect_to predicted_incomes_path
  end

  private

    def find_by_date(category,date)
      incomes = []
      incomes = Income.where(income_category: category.id,
                             date: (date.beginning_of_month..date.end_of_month))

      header = date.beginning_of_month.to_s + " - " + date.end_of_month.to_s
      predicted_income = PredictedIncome.where(income_category: category.id,
                                               beginning_of_month: date.beginning_of_month,
                                               end_of_month: date.end_of_month).first

      res = []
      return res << incomes << header << predicted_income
    end

    def predicted_income_params
      params.require(:predicted_income).permit(:amount)
    end

end
