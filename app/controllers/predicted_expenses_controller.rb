class PredictedExpensesController < ApplicationController

  def index
    @expense_categories = ExpenseCategory.where(user_id: current_user.id)
    @expenses_array = {}
    @predicted_expenses = {}
    @summa = 0
    @predicted_summa = 0
    if params[:date]
      @date = Date.new(params[:date][:year].to_i,
                      params[:date][:month].to_i)
    else
      @date = Date.today
    end
    @expense_categories.each do |category|
      category.amount = 0
      res = find_by_date(category,@date)
      @expenses_array[category.name], @header, @predicted_expenses[category.name] = res[0], res[1], res[2]
      @expenses_array[category.name].each {|expense| category.amount += expense.value}
      @summa += category.amount
      @predicted_summa += @predicted_expenses[category.name].amount if @predicted_expenses[category.name]
    end
  end

  def new
    @predicted_expense = PredictedExpense.new
    @category = ExpenseCategory.find(params[:category])
    date_params = params[:date].split("-")
    @date = Date.new(date_params[0].to_i, date_params[1].to_i, date_params[2].to_i)
  end

  def edit
    @predicted_expense = PredictedExpense.find(params[:id])
  end

  def create
    @predicted_expense = PredictedExpense.new(predicted_expense_params)
    @predicted_expense.expense_category_id = params[:category]
    date_params = params[:date].split("-")
    @date = Date.new(date_params[0].to_i, date_params[1].to_i, date_params[2].to_i)
    @predicted_expense.beginning_of_month = @date.beginning_of_month
    @predicted_expense.end_of_month = @date.end_of_month
    if @predicted_expense.save
      flash[:success] = "Предсказанный расход создан"
      redirect_to predicted_expenses_path
    else
      render 'new'
    end
  end

  def update
    @predicted_expense = PredictedExpense.find(params[:id])
    if @predicted_expense.update(predicted_expense_params)
      flash[:success] = "Предсказанный расход изменен"
      redirect_to predicted_expenses_path
    else
      render 'edit'
    end
  end

  def destroy
    @predicted_expenses = PredictedExpense.find(params[:id])
    @predicted_expenses.destroy

    flash[:success] = "Предсказаный расход удален"
    redirect_to predicted_expenses_path
  end

  private

    def find_by_date(category,date)
      expenses = []
      expenses = Expense.where(expense_category: category.id,
                             date: (date.beginning_of_month..date.end_of_month))

      header = date.beginning_of_month.to_s + " - " + date.end_of_month.to_s
      predicted_expense = PredictedExpense.where(expense_category: category.id,
                                               beginning_of_month: date.beginning_of_month,
                                               end_of_month: date.end_of_month).first

      res = []
      return res << expenses << header << predicted_expense
    end

    def predicted_expense_params
      params.require(:predicted_expense).permit(:amount)
    end

end
