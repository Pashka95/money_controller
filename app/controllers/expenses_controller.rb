class ExpensesController < ApplicationController

  def show
    @expense = Expense.find(params[:id])
    @expense_category = ExpenseCategory.find(params[:expense_category_id])
  end

  def new
    @expense = Expense.new
    @expense_category = ExpenseCategory.find(params[:expense_category_id])
    @accounts = current_user.accounts
  end

  def edit
    @expense = Expense.find(params[:id])
    @expense_category = ExpenseCategory.find(params[:expense_category_id])
  end

  def create
    @expense = Expense.new(expense_params)
    @expense.expense_category_id = params[:expense_category_id]
    @accounts = current_user.accounts
    if params[:expense][:account_id]
      @expense.account_id = params[:expense][:account_id]
    end
    @expense.date = Date.new(params[:start_date][:year].to_i,
                            params[:start_date][:month].to_i,
                            params[:start_date][:day].to_i)
    @expense_category = ExpenseCategory.find(params[:expense_category_id])
    if @expense.save
      if @expense.account_id
        @account = Account.find(@expense.account_id)
        @account.amount -= @expense.value
        @account.save
      end
      flash[:success] = "Расход добавлен"
      redirect_to expense_category_path(ExpenseCategory.find(params[:expense_category_id]))
    else
      render 'new'
    end
  end

  def update
    @expense = Expense.find(params[:id])
    prev_value = @expense.value
    @expense_category = ExpenseCategory.find(params[:expense_category_id])
    @expense.date = Date.new(params[:start_date][:year].to_i,
                            params[:start_date][:month].to_i,
                            params[:start_date][:day].to_i)
    if @expense.update(expense_params)
      if @expense.account_id
        @account = Account.find(@expense.account_id)
        @account.amount -= prev_value
        @account.amount += @expense.value
        @account.save
      end
      flash[:success] = "Расход изменен"
      redirect_to expense_category_path(ExpenseCategory.find(params[:expense_category_id]))
    else
      render 'edit'
    end
  end

  def destroy
    @expense = Expense.find(params[:id])
    if @expense.account_id
      @account = Account.find(@expense.account_id)
      @account.amount += @expense.value
      @account.save
    end
    @expense.destroy

    flash[:success] = "Расход удален"
    redirect_to expense_category_path(ExpenseCategory.find(params[:expense_category_id]))
  end

  private
    def expense_params
      params.require(:expense).permit(:description, :value, :date)
    end

end
