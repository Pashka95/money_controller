class ExpenseCategoryController < ApplicationController

  def index
    @expense_categories = ExpenseCategory.where(user_id: current_user.id)
    @expenses_array = {}
    @summa = 0
    date = Date.new(params[:start_date][:year].to_i, params[:start_date][:month].to_i, params[:start_date][:day].to_i) if params[:start_date]
    @expense_categories.each do |category|
      category.amount = 0
      res = find_by_date(category, date)
      @expenses_array[category.name], @header = res[0], res[1]
      @expenses_array[category.name].each {|expense| category.amount += expense.value}
      @summa += category.amount
    end
  end

  def show
    @expense_category = ExpenseCategory.find(params[:id])
    @expenses_array = []
    date = Date.new(params[:start_date][:year].to_i,
                    params[:start_date][:month].to_i,
                    params[:start_date][:day].to_i) if params[:start_date]
    @expense_category.amount = 0
    res = find_by_date(@expense_category, date)
    @expenses_array, @header = res[0], res[1]
    @expenses_array.each { |expense| @expense_category.amount += expense.value }
  end

  def new
    @expense_category = ExpenseCategory.new
  end

  def edit
    @expense_category = ExpenseCategory.find(params[:id])
  end

  def create
    @expense_category = ExpenseCategory.new(expense_category_params)
    @expense_category.user_id = current_user.id
    if @expense_category.save
      flash[:success] = "Категория #{@expense_category.name} создана"
      redirect_to expense_category_index_path
    else
      render 'new'
    end
  end

  def update
    @expense_category = ExpenseCategory.find(params[:id])

    if @expense_category.update(expense_category_params)
      flash[:success] = "Категория #{@expense_category.name} изменена"
      redirect_to expense_category_index_path
    else
      render 'edit'
    end
  end

  def destroy
    @expense_category = ExpenseCategory.find(params[:id])
    @expenses = Expense.where(expense_category: @expense_category.id)
    @expenses.each do |expense|
      if expense.account_id
        @account = Account.find(expense.account_id)
        @account.amount += expense.value
        @account.save
      end
    end

    @expense_category.destroy

    flash[:success] = "Категория удалена"
    redirect_to expense_category_index_path
  end

  def visual
    @expense_categories = ExpenseCategory.where(user_id: current_user.id)
    @expenses_array = {}
    @chart = {}
    date = Date.new(params[:start_date][:year].to_i, params[:start_date][:month].to_i, params[:start_date][:day].to_i) if params[:start_date]
    @expense_categories.each do |category|
      category.amount = 0
      res = find_by_date(category, date)
      @expenses_array[category.name], @header = res[0], res[1]
      @expenses_array[category.name].each {|expense| category.amount += expense.value}
      @chart[category.name] = category.amount
    end
  end

  private

    def find_by_date(category, date)
      array = []
      case(params[:period])
      when "1"
        array = Expense.where(expense_category: category.id,
                                      date: date)
        header = date
      when "2"
        array = Expense.where(expense_category: category.id,
                                      date: (date.beginning_of_week..date.end_of_week))
        header = date.beginning_of_week.to_s + " - " + date.end_of_week.to_s
      when "3"
        array = Expense.where(expense_category: category.id,
                                      date: (date.beginning_of_month..date.end_of_month))
        header = date.beginning_of_month.to_s + " - " + date.end_of_month.to_s
      when "4"
        array = Expense.where(expense_category: category.id,
                                      date: (date.beginning_of_year..date.end_of_year))
        header = date.beginning_of_year.to_s + " - " + date.end_of_year.to_s
      else
        date = Date.today unless params[:start_date]

        array = Expense.where(expense_category: category.id,
                                        date: date)
        header = date
      end
      res = []
      return res << array << header
    end

    def expense_category_params
      params.require(:expense_category).permit(:name)
    end

end
