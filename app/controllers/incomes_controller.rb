class IncomesController < ApplicationController

  def show
    @income = Income.find(params[:id])
    @income_category = IncomeCategory.find(params[:income_category_id])
  end

  def new
    @income = Income.new
    @income_category = IncomeCategory.find(params[:income_category_id])
    @accounts = current_user.accounts
  end

  def edit
    @income = Income.find(params[:id])
    @income_category = IncomeCategory.find(params[:income_category_id])
  end

  def create
    @accounts = current_user.accounts
    @income = Income.new(income_params)
    @income.income_category_id = params[:income_category_id]
    if params[:income][:account_id]
      @income.account_id = params[:income][:account_id]
    end
    @income.date = Date.new(params[:start_date][:year].to_i,
                            params[:start_date][:month].to_i,
                            params[:start_date][:day].to_i)
    @income_category = IncomeCategory.find(params[:income_category_id])
    if @income.save
      if @income.account_id
        @account = Account.find(@income.account_id)
        @account.amount += @income.value
        @account.save
      end
      flash[:success] = "Доход добавлен"
      redirect_to income_category_path(IncomeCategory.find(params[:income_category_id]))
    else
      render 'new'
    end
  end

  def update
    @income = Income.find(params[:id])
    prev_value = @income.value
    @income_category = IncomeCategory.find(params[:income_category_id])
    @income.date = Date.new(params[:start_date][:year].to_i,
                            params[:start_date][:month].to_i,
                            params[:start_date][:day].to_i)
    if @income.update(income_params)
      if @income.account_id
        @account = Account.find(@income.account_id)
        @account.amount -= prev_value
        @account.amount += @income.value
        @account.save
      end
      flash[:success] = "Доход изменен"
      redirect_to income_category_path(IncomeCategory.find(params[:income_category_id]))
    else
      render 'edit'
    end
  end

  def destroy
    @income = Income.find(params[:id])
    if @income.account_id
      @account = Account.find(@income.account_id)
      @account.amount -= @income.value
      @account.save
    end
    @income.destroy

    flash[:success] = "Доход удален"
    redirect_to income_category_path(IncomeCategory.find(params[:income_category_id]))
  end

  private
    def income_params
      params.require(:income).permit(:description, :value, :date)
    end

end
