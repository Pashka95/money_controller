class IncomeCategoryController < ApplicationController

  def index
    @income_categories = IncomeCategory.where(user_id: current_user.id)
    @incomes_array = {}
    @summa = 0
    date = Date.new(params[:start_date][:year].to_i, params[:start_date][:month].to_i, params[:start_date][:day].to_i) if params[:start_date]
    @income_categories.each do |category|
      category.amount = 0
      res = find_by_date(category, date)
      @incomes_array[category.name], @header = res[0], res[1]
      @incomes_array[category.name].each {|income| category.amount += income.value}
      @summa += category.amount
    end
  end

  def show
    @income_category = IncomeCategory.find(params[:id])
    @incomes_array = []
    date = Date.new(params[:start_date][:year].to_i,
                    params[:start_date][:month].to_i,
                    params[:start_date][:day].to_i) if params[:start_date]
    @income_category.amount = 0
    res = find_by_date(@income_category, date)
    @incomes_array, @header = res[0], res[1]
    @incomes_array.each { |income| @income_category.amount += income.value }
  end

  def new
    @income_category = IncomeCategory.new
  end

  def edit
    @income_category = IncomeCategory.find(params[:id])
  end

  def create
    @income_category = IncomeCategory.new(income_category_params)
    @income_category.user_id = current_user.id
    if @income_category.save
      flash[:success] = "Категория #{@income_category.name} создана"
      redirect_to income_category_index_path
    else
      render 'new'
    end
  end

  def update
    @income_category = IncomeCategory.find(params[:id])

    if @income_category.update(income_category_params)
      flash[:success] = "Категория #{@income_category.name} изменена"
      redirect_to income_category_index_path
    else
      render 'edit'
    end
  end

  def destroy
    @income_category = IncomeCategory.find(params[:id])
    @incomes = Income.where(income_category: @income_category.id)
    @incomes.each do |income|
      if income.account_id
        @account = Account.find(income.account_id)
        @account.amount -= income.value
        @account.save
      end
    end
    @income_category.destroy

    flash[:success] = "Категория удалена"
    redirect_to income_category_index_path
  end

  def visual
    @income_categories = IncomeCategory.where(user_id: current_user.id)
    @incomes_array = {}
    @chart = {}
    date = Date.new(params[:start_date][:year].to_i, params[:start_date][:month].to_i, params[:start_date][:day].to_i) if params[:start_date]
    @income_categories.each do |category|
      category.amount = 0
      res = find_by_date(category, date)
      @incomes_array[category.name], @header = res[0], res[1]
      @incomes_array[category.name].each {|income| category.amount += income.value}
      @chart[category.name] = category.amount
    end
  end

  private

    def find_by_date(category, date)
      array = []
      case(params[:period])
      when "1"
        array = Income.where(income_category: category.id,
                                      date: date)
        header = date
      when "2"
        array = Income.where(income_category: category.id,
                                      date: (date.beginning_of_week..date.end_of_week))
        header = date.beginning_of_week.to_s + " - " + date.end_of_week.to_s
      when "3"
        array = Income.where(income_category: category.id,
                                      date: (date.beginning_of_month..date.end_of_month))
        header = date.beginning_of_month.to_s + " - " + date.end_of_month.to_s
      when "4"
        array = Income.where(income_category: category.id,
                                      date: (date.beginning_of_year..date.end_of_year))
        header = date.beginning_of_year.to_s + " - " + date.end_of_year.to_s
      else
        date = Date.today unless params[:start_date]

        array = Income.where(income_category: category.id,
                                        date: date)
        header = date
      end
      res = []
      return res << array << header
    end

    def income_category_params
      params.require(:income_category).permit(:name)
    end

end
