class Expense < ActiveRecord::Base
  belongs_to  :expense_category
  validates   :value, numericality: { greater_than: 0 }
end
