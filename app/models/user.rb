class User < ActiveRecord::Base
  has_many :income_categories, dependent: :destroy
  has_many :accounts, dependent: :destroy

  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }

  before_save { self.email = email.downcase }
  before_create :create_remember_token
  after_create :create_categories_and_accounts


  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  has_secure_password

  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end

    def create_categories_and_accounts
      income_category_name = ["Депозит", "Зарплата", "Сбережения"]
      income_category_name.each { |name| IncomeCategory.create(name: name, amount: 0, user_id: self.id) }

      expense_category_name = ["Счета", "Машина", "Одежда", "Телефон", "Продукты", "Рестораны", "Развлечения", "Подарки", "Здоровье", "Дом", "Спорт", "Питомцы", "Такси", "Гигиена", "Транспорт"]
      expense_category_name.each { |name| ExpenseCategory.create(name: name,  amount: 0, user_id: self.id) }

      accounts_name = ["Кошелек", "Карта для оплаты"]
      accounts_name.each { |name| Account.create(name: name, amount: 0, user_id: self.id) }
     end
end
