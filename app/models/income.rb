class Income < ActiveRecord::Base
  belongs_to  :income_category
  validates   :value, numericality: { greater_than: 0 }
end
