class Account < ActiveRecord::Base
  belongs_to  :user
  validates   :name,   presence: true
  validates   :amount, numericality: true, presence: true
  validate    :uniqueness_name, on: [:create, :update]

  def uniqueness_name
    accounts = Account.where(user_id: self.user_id)
    accounts.each do |account|
      errors.add(:Название, "уже существует") if self.name == account.name and self.id != account.id
    end
  end
end
