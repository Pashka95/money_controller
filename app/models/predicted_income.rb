class PredictedIncome < ActiveRecord::Base
  belongs_to  :income_category
  validates   :amount, numericality: { greater_than_or_equal_to: 0 }
end
