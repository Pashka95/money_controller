class ExpenseCategory < ActiveRecord::Base
  belongs_to  :user
  has_many    :expenses, dependent: :destroy
  has_many    :predicted_expenses, dependent: :destroy
  validates   :name, presence: true
  validate    :uniqueness_name, on: [:create, :update]

  def uniqueness_name
    categories = ExpenseCategory.where(user_id: self.user_id)
    categories.each do |category|
      errors.add(:Название, "уже существует") if self.name == category.name and self.id != category.id
    end
  end
end
