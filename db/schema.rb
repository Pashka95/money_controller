# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160131143746) do

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id"

  create_table "expense_categories", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "expense_categories", ["user_id"], name: "index_expense_categories_on_user_id"

  create_table "expenses", force: :cascade do |t|
    t.string   "description"
    t.float    "value"
    t.date     "date"
    t.integer  "expense_category_id"
    t.integer  "account_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "expenses", ["expense_category_id"], name: "index_expenses_on_expense_category_id"

  create_table "income_categories", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "income_categories", ["user_id"], name: "index_income_categories_on_user_id"

  create_table "incomes", force: :cascade do |t|
    t.string   "description"
    t.float    "value"
    t.date     "date"
    t.integer  "income_category_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "account_id"
  end

  add_index "incomes", ["income_category_id"], name: "index_incomes_on_income_category_id"

  create_table "predicted_expenses", force: :cascade do |t|
    t.integer  "expense_category_id"
    t.float    "amount"
    t.date     "beginning_of_month"
    t.date     "end_of_month"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "predicted_expenses", ["expense_category_id"], name: "index_predicted_expenses_on_expense_category_id"

  create_table "predicted_incomes", force: :cascade do |t|
    t.integer  "income_category_id"
    t.float    "amount"
    t.date     "beginning_of_month"
    t.date     "end_of_month"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "predicted_incomes", ["income_category_id"], name: "index_predicted_incomes_on_income_category_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_token"
    t.boolean  "admin"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["remember_token"], name: "index_users_on_remember_token"

end
