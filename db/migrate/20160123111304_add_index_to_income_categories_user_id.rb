class AddIndexToIncomeCategoriesUserId < ActiveRecord::Migration
  def change
    add_index :income_categories, :user_id
  end
end
