class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string  :description
      t.float   :value
      t.date    :date
      t.integer :expense_category_id
      t.integer :account_id

      t.timestamps null: false
    end
    add_index :expenses, :expense_category_id
  end
end
