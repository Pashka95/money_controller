class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.string  :description
      t.float   :value
      t.date    :date
      t.integer :income_category_id

      t.timestamps null: false
    end
  end
end
