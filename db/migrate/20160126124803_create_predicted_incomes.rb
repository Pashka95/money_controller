class CreatePredictedIncomes < ActiveRecord::Migration
  def change
    create_table :predicted_incomes do |t|
      t.integer   :income_category_id
      t.float     :amount
      t.date      :beginning_of_month
      t.date      :end_of_month

      t.timestamps null: false
    end
    add_index :predicted_incomes, :income_category_id
  end
end
