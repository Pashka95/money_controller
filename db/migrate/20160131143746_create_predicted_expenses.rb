class CreatePredictedExpenses < ActiveRecord::Migration
  def change
    create_table :predicted_expenses do |t|
      t.integer   :expense_category_id
      t.float     :amount
      t.date      :beginning_of_month
      t.date      :end_of_month

      t.timestamps null: false
    end
    add_index :predicted_expenses, :expense_category_id
  end
end
