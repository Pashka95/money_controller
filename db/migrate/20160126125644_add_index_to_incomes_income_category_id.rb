class AddIndexToIncomesIncomeCategoryId < ActiveRecord::Migration
  def change
    add_index :incomes, :income_category_id
  end
end
