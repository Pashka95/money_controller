class CreateExpenseCategories < ActiveRecord::Migration
  def change
    create_table :expense_categories do |t|
      t.string  :name
      t.float   :amount
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :expense_categories, :user_id
  end
end
